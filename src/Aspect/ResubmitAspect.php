<?php

declare(strict_types=1);

namespace Xho\Aspect;

use Hyperf\Di\Annotation\Aspect;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\Exception\Exception;
use Hyperf\Logger\LoggerFactory;
use Hyperf\Redis\Redis;
use Xho\Annotation\Resubmit;
use Xho\Exception\MiniException;
use Xho\Exception\NormalStatusException;
use Xho\XhoRequest;
use Xho\Redis\MiniLockRedis;

use function Hyperf\Support\make;

/**
 * Class ResubmitAspect.
 */
#[Aspect]
class ResubmitAspect extends AbstractAspect
{
    public array $annotations = [
        Resubmit::class,
    ];

    /**
     * @return mixed
     * @throws Exception
     * @throws \Throwable
     */
    public function process(ProceedingJoinPoint $proceedingJoinPoint)
    {
        try {
            /* @var $resubmit Resubmit */
            if (isset($proceedingJoinPoint->getAnnotationMetadata()->method[Resubmit::class])) {
                $resubmit = $proceedingJoinPoint->getAnnotationMetadata()->method[Resubmit::class];
            }

            $request = container()->get(XhoRequest::class);

            $key = md5(sprintf('%s-%s-%s', $request->ip(), $request->getPathInfo(), $request->getMethod()));

            $lockRedis = new MiniLockRedis(
                make(Redis::class),
                make(LoggerFactory::class)->get('Mini Redis Lock')
            );
            $lockRedis->setTypeName('resubmit');

            if ($lockRedis->check($key)) {
                $lockRedis = null;
                throw new NormalStatusException($resubmit->message ?: t('miniadmin.resubmit'), 500);
            }

            $lockRedis->lock($key, $resubmit->second);
            $lockRedis = null;

            return $proceedingJoinPoint->process();
        } catch (\Throwable $e) {
            throw new MiniException($e->getMessage(), $e->getCode());
        }
    }
}
