<?php

declare(strict_types=1);

namespace Xho;

use Xho\Traits\ControllerTrait;

/**
 * API接口控制器基类
 * Class MiniApi.
 */
abstract class XhoApi
{
    use ControllerTrait;

    public function __construct(
        readonly protected XhoRequest $request,
        readonly protected XhoResponse $response
    ) {}

    public function getResponse(): XhoResponse
    {
        return $this->response;
    }

    public function getRequest(): XhoRequest
    {
        return $this->request;
    }
}
