<?php
declare (strict_types=1);

namespace Xho;


use Xho\Traits\ControllerTrait;

abstract class XhoController
{
    use ControllerTrait;

    public function __construct(
        readonly protected Xho $mini,
        readonly protected XhoRequest $request,
        readonly protected XhoResponse $response
    ) {}

    public function getResponse(): XhoResponse
    {
        return $this->response;
    }

    public function getRequest(): XhoRequest
    {
        return $this->request;
    }
}