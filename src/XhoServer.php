<?php

declare(strict_types=1);

namespace Xho;

use Hyperf\HttpServer\Server;

class XhoServer extends Server
{
    protected ?string $serverName = 'MiniAdmin';

    protected $routes;

    public function onRequest($request, $response): void
    {
        parent::onRequest($request, $response);
        $this->bootstrap();
    }

    /**
     * MiniServer bootstrap.
     */
    protected function bootstrap(): void {}
}
