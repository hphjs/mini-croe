<?php

declare(strict_types=1);

namespace Xho\Exception;

class UserBanException extends MiniException {}
