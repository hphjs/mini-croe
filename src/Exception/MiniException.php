<?php

declare(strict_types=1);

namespace Xho\Exception;

class MiniException extends \RuntimeException {}
