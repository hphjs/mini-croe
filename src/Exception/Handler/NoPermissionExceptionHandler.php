<?php

declare(strict_types=1);

namespace Xho\Exception\Handler;

use Hyperf\Codec\Json;
use Hyperf\ExceptionHandler\ExceptionHandler;
use Hyperf\HttpMessage\Stream\SwooleStream;
use Hyperf\Validation\ValidationException;
use Xho\Exception\NoPermissionException;
use Xho\Helper\XhoCode;
use Xho\Log\RequestIdHolder;
use Xho\XhoRequest;
use Psr\Http\Message\ResponseInterface;

/**
 * Class NoPermissionExceptionHandler.
 */
class NoPermissionExceptionHandler extends ExceptionHandler
{
    public function handle(\Throwable $throwable, ResponseInterface $response): ResponseInterface
    {
        $this->stopPropagation();
        $format = [
            'requestId' => RequestIdHolder::getId(),
//            'path' => container()->get(MiniRequest::class)->getUri()->getPath(),
            'success' => false,
            'message' => $throwable->getMessage(),
            'code' => XhoCode::NO_PERMISSION,
        ];
        return $response->withHeader('Server', 'MineAdmin')
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'accept-language,authorization,lang,uid,token,Keep-Alive,User-Agent,Cache-Control,Content-Type')
            ->withAddedHeader('content-type', 'application/json; charset=utf-8')
            ->withStatus(403)->withBody(new SwooleStream(Json::encode($format)));
    }

    public function isValid(\Throwable $throwable): bool
    {
        return $throwable instanceof NoPermissionException;
    }
}
