<?php

declare(strict_types=1);

namespace Xho\Interfaces\ServiceInterface;

use Xho\Interfaces\ServiceInterface\Resource\BaseResource;
use Xho\Interfaces\ServiceInterface\Resource\DataResource;

interface DataResourceServiceInterface extends BaseResource, DataResource {}
