<?php

declare(strict_types=1);

namespace Xho\Interfaces\ServiceInterface;

use Xho\Interfaces\ServiceInterface\Resource\BaseResource;
use Xho\Interfaces\ServiceInterface\Resource\FieldValueResource;
use Xho\Interfaces\ServiceInterface\Resource\QueryResource;

interface QueryResourceServiceInterface extends BaseResource, QueryResource, FieldValueResource {}
